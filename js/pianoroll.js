//
// JavaScript Music Trainer
// (C) Jacques Mattheij 2020; jacques@modularcompany.com
// All rights reserved, 
//
// This source file deals with creating and maintaining
// a pianoroll like structure that becomes embedded
// in the score. 
// 
// midi score onto a canvas as well as creating a lot of 
// useful meta data about the score as rendered.
//

// Pianoroll representation of the midi file. This is useful because it allows us to check if a combination of
// notes is permitted rather than required at any point in the piece. It uses the duration of the notes as 
// guide to determine where notes may still be sounding
  
class Pianoroll {
        // set up an array of zeros per millisecond in the piece

        constructor(seconds)

        {
                this.fractions_per_sec = 50; // n fractions per second
                this.ms_per_frac = 1000 / this.fractions_per_sec;

                var n_fractions = seconds * this.fractions_per_sec;

                this.roll = [];

                console.log("creating a pianoroll of ", seconds, "seconds", n_fractions, " slots long");

                for (var i=0;i<n_fractions;i++) {
                        this.roll.push("00000000000000000000000000000000");
                }
        }

      
        ms_to_frac(milliseconds)

        {
                return Math.floor(milliseconds / this.ms_per_frac);
        }

        // I shudder to think about how ineffecient this code is compared
        // to the equivalent machine language version. 

        set(milliseconds, midi)

        {
                var slot = this.ms_to_frac(milliseconds);

                console.assert(slot < this.roll.length);

                var s = this.roll[slot];

                var c = 31 - (midi >> 2);
                var b = 1 << (midi % 4);

                var left = s.substr(0,c);
                var right = s.substr(c+1);

                var chr = s.substr(c,1);

                var i = parseInt(chr, 16);

                i = i | b;

                var h = Number(i).toString(16);

                this.roll[slot] = left + h + right;
        }

        range_set(from, to, midi)

        {
                to = Math.floor(to);

                for (var i=Math.floor(from);i<to;i++) {
                        this.set(i, midi);
                }
        }

        is_set(milliseconds, midi)

        {
                var slot = this.ms_to_frac(milliseconds);

                console.assert(slot < this.roll.length);

                var s = this.roll[slot];

                var c = 31 - (midi >> 2);
                var b = 1 << (midi % 4);

                var chr = s.substr(c,1);

                var i = parseInt(chr, 16);

                return (i & b) != 0;
        }
}

pianoroll = new Pianoroll(10);
pianoroll.set(5555, 64);
pianoroll.set(5555, 3);

console.log(pianoroll);

console.log(pianoroll.is_set(5555, 65));
console.log(pianoroll.is_set(5555, 64));
console.log(pianoroll.is_set(5555, 63));
console.log(pianoroll.is_set(5555, 3));
console.log(pianoroll.is_set(5556, 65));

delete pianoroll;

// debugger;

